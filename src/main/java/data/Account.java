package data;

import interaction.Role;

public class Account {
	Role role;

	public Role getRole() {
		return this.role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public void execute() {
		this.role.execute();
	}
}
