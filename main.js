var transferMoneyContext = require('./context/transfer-money-context'),
    accountManagerContext = require('./context/account-manager-context');

transferMoneyUseCase();

changeAccountOwnerUseCase();

lockAccountUseCase();

function transferMoneyUseCase() {
    transferMoneyContext.initialize('acc1', 'acc2', 100);
    transferMoneyContext.doTransfer();
}

function changeAccountOwnerUseCase(){
    accountManagerContext.initialize('acc1');
    accountManagerContext.changeOwner('bob69')

}

function lockAccountUseCase(){
    accountManagerContext.initialize('acc1');
    accountManagerContext.lockAccount();
}




