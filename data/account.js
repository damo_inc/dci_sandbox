function Account() {
    return {
        find:findAcc
    }
}


function findAcc(accId){
    return {
        id: accId,
        balance: 100,
        isLocked: false,
        owner: {
            id: 'bob1',
            name: 'Bob' + Math.random()
        }
    }
}

module.exports = Account();
