function User() {
    return {
        find: findUser
    }
}


function findUser(userId) {
    return {
        id: userId,
        name: 'Bob ' + Math.random()
    }
}

module.exports = User();
