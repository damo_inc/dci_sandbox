var Account = require('./../data/account'),
    sourceRole = require('./../interaction/source-account-role'),
    destinationRole = require('./../interaction/destination-account-role'),
    sourceAcc,
    destinationAcc,
    transferSum;

function TransferContext() {
    return {
        initialize: initialize,
        doTransfer: doTransfer
    }
}

function initialize(sourceAccId, destAccId, amount) {
    transferSum = amount;
    sourceAcc = Account.find(sourceAccId);
    destinationAcc = Account.find(destAccId);
//    sourceAcc.isLocked = true;
//    destinationAcc.isLocked = true;
}

function doTransfer() {
    try {
        sourceRole.call(sourceAcc, transferSum);
        destinationRole.call(destinationAcc, transferSum);
    } catch (err) {
        console.log('rollback!!', err);
    }
}

module.exports = TransferContext();