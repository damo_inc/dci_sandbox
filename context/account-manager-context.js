var Account = require('./../data/account'),
    managerRole = require('./../interaction/account-manager-role'),
    account;

function ManagerContext() {
    return {
        initialize: initialize,
        changeOwner: changeOwner,
        lockAccount: lockAccount
    }
}

function initialize(accId) {
    account = Account.find(accId);
}

function changeOwner(newOwnerId) {
    try {
        managerRole.changeOwner.call(account, newOwnerId);
    } catch (err) {
        console.log('rollback!!', err);
    }
}


function lockAccount() {
    try {
        managerRole.lockAccount.call(account);
    } catch (err) {
        console.log('rollback!!', err);
    }
}
module.exports = ManagerContext();