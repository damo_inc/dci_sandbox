var User = require('./../data/user'),
    account;

function changeOwner(userId) {
    account = this;
    var newGuy = getUser(userId);
    this.owner = newGuy;
    console.log('Owner changed to: ' + account.owner.id);
}

function freezeAccount() {
    account = this;
    this.isLocked = true;
    console.log('Account: ' +account.id + ' was locked.');
}

function getUser(userId) {
    var user = User.find(userId);

    if (!user) {
        throw new Error('No such user exists. Operation terminated.');
    }
    return user;
}

module.exports = {
    changeOwner: changeOwner,
    lockAccount: freezeAccount
};