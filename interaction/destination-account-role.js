var account;

function receiveMoney(transferSum) {
    account = this;
    validateNotLocked();
    this.balance += transferSum;
    console.log('Transfer complete: dest acc balance: ' + account.balance);
}

function validateNotLocked(){
    if(account.isLocked)
        throw new Error('Destination Account is locked. Operation terminated.');
}

module.exports = receiveMoney;