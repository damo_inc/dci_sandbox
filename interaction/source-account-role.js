var account;

function sendMoney(transferSum) {
    account = this;
    validateNotLocked();
    validateAvailableBalance(transferSum);
    this.balance -= transferSum;
    console.log('Transfer in-progress: source account balance: ' + account.balance);
}

function validateAvailableBalance(transferSum){
    if(account.balance < transferSum){
        throw new Error("Not Enough funds in account. Operation terminated.");
    }
}

function validateNotLocked(){

    if(account.isLocked) {
        throw new Error('Source Account is locked. Operation terminated.');
    }
}

module.exports = sendMoney;